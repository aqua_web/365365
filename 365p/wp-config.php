<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'ae150ffhe5_wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'ae150ffhe5');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'e4nznAcS');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':twH~kjMa_7Z67Ht=m@;br4`tHPp_WB#KXST{BnxLaAgpS0bXt?iw{@HCu{w5hHt');
define('SECURE_AUTH_KEY',  '!T!7`~MU%]OHIZvz*PL:ges0Vkn{/k7s{&}v.hcsRjatZB@>L$P#!PY4;S_ZZ[UF');
define('LOGGED_IN_KEY',    '#HH(#rrrbvG.cTNfJ~<4ebi|I/lA]`r>K}%iP0UM&cg;+%jrF} }GJO&[3$GEA#7');
define('NONCE_KEY',        '1_XN4zzto[Wg~w e0REZ|bgRo7HC_(7?M^{_A7bwT(mE}ZsH)*T@[@S8pn%-sT>[');
define('AUTH_SALT',        'Lf z6yy9+!DFb<(VWyCiUY`1BZgUznIT~5MjT+^: }*@ei*IJM(ssNc+n6M z,%m');
define('SECURE_AUTH_SALT', '[)flUet2uyhh_=^` Oqx](!0XNbbQFZv3RwMYI.PGui<t-?pYh2fk)l=b9sBF[^g');
define('LOGGED_IN_SALT',   'xx#O2X<YMJ`ocu,fVn:CgnW$.[h).ep`VB$<JVrd>0oB<S!a@T(|eC.>djJ50THx');
define('NONCE_SALT',       '3.1W-p,&4Ip%wF(Is[.33ZJH%NoK`)C-3%m!YyB-aXE^)PEtt8*`5FVa(S,<`.&G');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
