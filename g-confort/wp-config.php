<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'ae150ffhe5_wordpress0002');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'ae150ffhe5');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'e4nznAcS');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'P?Mx6bkke&f?#6!Dc.wb.G(c&f6F,?Ma-|+7r3V)M),SDjcd2/3ASY^m&+~v!),?');
define('SECURE_AUTH_KEY',  'py[]V]V;15~So/`Wh%Hk/N4]vNjeC;QJ{RPJ~1)2w.A[j;qmXy6UV~jioDQEzLQ}');
define('LOGGED_IN_KEY',    'N{4CHMjzh:dEDVX]}?I`%Ms5|eCrs@?*5M3W?-$9nI;SPr?Z:yDh+z=)C2[P%.Hn');
define('NONCE_KEY',        'wUni!_<?g g{EUUs?BI|Yb79B0B &mnz/f~B>f%zyyfDG3,u|?OC_~Mlq*oi15Y]');
define('AUTH_SALT',        ',x%8Z$ba`4?spcI5.KErEH4`BQ2adwm34oDr[C_rW&nH81m{To`<=)g <KV[.{9F');
define('SECURE_AUTH_SALT', '$jbd# I/.@?Drt@)k{ ]^QS4b1ki=rv9X##15MZz{{4n=nNA(h^<5lF~1__7ONec');
define('LOGGED_IN_SALT',   'm]H6A17)kBaiR`EYG:e({a2r/4KNNdv)rB3#xdZ]Q5.wx@VQ,sSTfVFcNEvZh?(4');
define('NONCE_SALT',       ';QIMOCF>8:V:8OJKZF2M{;,z4> c1km}hd$?ze$!)xclYSdNJSF16uoHC[7nyrj<');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
