<?php
/**
 * Template Name: Full width
 * Template Post Type: plan
 */
__( 'Full width', 'tcd-w' );
$args = array(
	'post_status' => 'publish',
	'post_type' => 'plan',
	'posts_per_page' => -1
);
$the_query = new WP_Query( $args );
get_header();
?>
<main class="l-main">	
	<?php get_template_part( 'template-parts/page-header' ); ?>
	<?php get_template_part( 'template-parts/breadcrumb' ); ?>
	<?php
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();
	?>
	<div class="p-entry__body">
		<?php
		the_content();
		wp_link_pages( array( 
			'before' => '<div class="p-page-links">', 
			'after' => '</div>', 
			'link_before' => '<span>', 
			'link_after' => '</span>' 
		) ); 
		?>
	</div>
	<?php
		endwhile;
	endif;
	// get_template_part( 'template-parts/plan-list' );
	?>
<div id="js-contents-builder">	
	<div id="cb_2" class="p-content03">
		<section class="p-content03__blog u-clearfix">
			<div class="p-content03__blog-header">
				<h2 class="p-content03__blog-catch" style="font-size: 40px;">セミナー情報</h2>
				<a class="p-content03__blog-archive-link" href="http://ae150ffhe5.smartrelease.jp/g-confort"></a>
			</div>
							<div class="p-content03__blog-list">
				<div class="p-content03__blog-list-inner slick-initialized slick-slider">
											
											
											
										<div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 843px; transform: translate3d(0px, 0px, 0px);"><article class="p-content03__blog-list-item p-article04 slick-slide slick-current slick-active" style="width: 281px;" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00">
						<a class="p-article04__thumbnail p-hover-effect--type1" href="http://ae150ffhe5.smartrelease.jp/g-confort/2017/11/22/%e3%83%86%e3%82%b9%e3%83%883/" tabindex="0">
							<img width="480" height="320" src="http://ae150ffhe5.smartrelease.jp/g-confort/wp-content/uploads/2017/11/img_7-480x320.jpg" class="attachment-size1 size-size1 wp-post-image" alt="">							</a>
						<h3 class="p-article04__title"><a href="http://ae150ffhe5.smartrelease.jp/g-confort/2017/11/22/%e3%83%86%e3%82%b9%e3%83%883/" tabindex="0">テスト3</a></h3>
						<p class="p-article04__excerpt">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト...</p>
													<p class="p-article04__meta"><time class="p-article04__date" datetime="2017-11-22">2017.11.22</time><span class="p-article04__category"><a href="http://ae150ffhe5.smartrelease.jp/g-confort/category/%e6%9c%aa%e5%88%86%e9%a1%9e/" rel="category tag" tabindex="0">未分類</a></span></p>
												</article><article class="p-content03__blog-list-item p-article04 slick-slide slick-active" style="width: 281px;" data-slick-index="1" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide01">
						<a class="p-article04__thumbnail p-hover-effect--type1" href="http://ae150ffhe5.smartrelease.jp/g-confort/2017/11/22/%e3%83%86%e3%82%b9%e3%83%882/" tabindex="0">
							<img width="480" height="320" src="http://ae150ffhe5.smartrelease.jp/g-confort/wp-content/uploads/2017/11/img_7-480x320.jpg" class="attachment-size1 size-size1 wp-post-image" alt="">							</a>
						<h3 class="p-article04__title"><a href="http://ae150ffhe5.smartrelease.jp/g-confort/2017/11/22/%e3%83%86%e3%82%b9%e3%83%882/" tabindex="0">テスト2</a></h3>
						<p class="p-article04__excerpt">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト...</p>
													<p class="p-article04__meta"><time class="p-article04__date" datetime="2017-11-22">2017.11.22</time><span class="p-article04__category"><a href="http://ae150ffhe5.smartrelease.jp/g-confort/category/%e6%9c%aa%e5%88%86%e9%a1%9e/" rel="category tag" tabindex="0">未分類</a></span></p>
												</article><article class="p-content03__blog-list-item p-article04 slick-slide slick-active" style="width: 281px;" data-slick-index="2" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide02">
						<a class="p-article04__thumbnail p-hover-effect--type1" href="http://ae150ffhe5.smartrelease.jp/g-confort/2017/11/22/%e3%83%86%e3%82%b9%e3%83%881/" tabindex="0">
							<img width="480" height="320" src="http://ae150ffhe5.smartrelease.jp/g-confort/wp-content/uploads/2017/11/img_7-480x320.jpg" class="attachment-size1 size-size1 wp-post-image" alt="">							</a>
						<h3 class="p-article04__title"><a href="http://ae150ffhe5.smartrelease.jp/g-confort/2017/11/22/%e3%83%86%e3%82%b9%e3%83%881/" tabindex="0">テスト1</a></h3>
						<p class="p-article04__excerpt">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト...</p>
													<p class="p-article04__meta"><time class="p-article04__date" datetime="2017-11-22">2017.11.22</time><span class="p-article04__category"><a href="http://ae150ffhe5.smartrelease.jp/g-confort/category/%e6%9c%aa%e5%88%86%e9%a1%9e/" rel="category tag" tabindex="0">未分類</a></span></p>
												</article></div></div></div>
			</div>
							<div class="p-content03__blog-arrows">
			</div>
		</section>
		<section class="p-content03__news" style="background: #222222">
			<h2 class="p-content03__news-catch" style="font-size: 40px;">NEWS</h2>
							<div class="p-content03__news-list">
									<article class="p-content03__news-list-item p-article05">
					<a href="http://ae150ffhe5.smartrelease.jp/g-confort/news/%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88-4/">
													<time class="p-article05__date" datetime="2017-11-22">2017.11.22</time>
													<h3 class="p-article05__title">テキストテキストテキストテキストテキスト</h3>
					</a>
				</article>
									<article class="p-content03__news-list-item p-article05">
					<a href="http://ae150ffhe5.smartrelease.jp/g-confort/news/%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88-3/">
													<time class="p-article05__date" datetime="2017-11-22">2017.11.22</time>
													<h3 class="p-article05__title">テキストテキストテキストテキストテキスト</h3>
					</a>
				</article>
									<article class="p-content03__news-list-item p-article05">
					<a href="http://ae150ffhe5.smartrelease.jp/g-confort/news/%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88%e3%83%86%e3%82%ad%e3%82%b9%e3%83%88-2/">
													<time class="p-article05__date" datetime="2017-11-22">2017.11.22</time>
													<h3 class="p-article05__title">テキストテキストテキストテキストテキスト</h3>
					</a>
				</article>
								</div>
							<a class="p-content03__news-archive-link" href="http://ae150ffhe5.smartrelease.jp/g-confort/news/"></a>
		</section>
	</div>
</div>
</main>
<?php get_footer(); ?>
